package net.suby.failsaferetry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class FailsafeRetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(FailsafeRetryApplication.class, args);
    }

}
