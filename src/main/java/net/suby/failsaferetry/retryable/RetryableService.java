package net.suby.failsaferetry.retryable;

import java.util.Random;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class RetryableService {

    @Retryable(maxAttempts = 2, backoff = @Backoff(2000), value = IllegalStateException.class,
            exclude = { NullPointerException.class, NullPointerException.class })
    public String getRetryable(Integer intValue) {
        int rand = new Random().nextInt();
        if (!ObjectUtils.isEmpty(intValue)) {
            rand = intValue;
        }

        if (rand < 0) {
            throw new NullPointerException("값이 음수일수 없습니다.");
        } else if (rand == 0) {
            throw new NumberFormatException("0의 값입니다.");
        } else if (rand % 3 != 0) {
            throw new IllegalStateException("fail retry");
        }

        return String.format("성공 : %s", rand);
    }

    @Recover
    String recover(NullPointerException e) {
        return e.getMessage();
    }

    @Recover
    String recover(NumberFormatException e, Integer intValue) {
        return String.format("%s : %s", e.getMessage(), intValue);
    }
}
