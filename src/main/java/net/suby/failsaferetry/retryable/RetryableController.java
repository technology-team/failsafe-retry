package net.suby.failsaferetry.retryable;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RetryableController {

    private final RetryableService retryableService;

    @GetMapping("/retryable")
    public String getRetryable(@RequestParam(required = false) Integer intValue) {
        return retryableService.getRetryable(intValue);
    }
}
