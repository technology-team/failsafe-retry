package net.suby.failsaferetry.failsafe;

import java.time.Duration;
import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

@Service
public class FailSafeService {

    public String getRetryPolicy(Integer intValue) {
        RetryPolicy<Object> retryPolicy = new RetryPolicy<>().handle(IllegalStateException.class, NumberFormatException.class,
                                                                     NullPointerException.class)
                                                             .withDelay(Duration.ofSeconds(5))
                                                             .handleResultIf(result -> result == "fail")
                                                             .withMaxRetries(3);
        return Failsafe.with(retryPolicy).get(() -> getRetryable(intValue));
    }

    private String getRetryable(Integer intValue) {
        int rand = new Random().nextInt();
        if (!ObjectUtils.isEmpty(intValue)) {
            rand = intValue;
        }

        if (rand < 0) {
            throw new NullPointerException("값이 음수일수 없습니다.");
        } else if (rand == 0) {
            throw new NumberFormatException("0의 값입니다.");
        } else if (rand % 3 != 0) {
            throw new IllegalStateException("fail retry");
        }

        return String.format("성공 : %s", rand);
    }
}
