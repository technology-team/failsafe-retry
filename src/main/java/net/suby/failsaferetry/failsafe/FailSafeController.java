package net.suby.failsaferetry.failsafe;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class FailSafeController {

    private final FailSafeService failSafeService;

    @GetMapping("/fail-safe")
    public String getRetryable(@RequestParam(required = false) Integer intValue) {
        return failSafeService.getRetryPolicy(intValue);
    }
}
